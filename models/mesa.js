const mongoose = require('mongoose');

const mesaSchema = new mongoose.Schema({
    numero: {
        type: String,
        required: true
    },
    situacao: String
}, {timestamps: true});
const mesa = mongoose.model('mesas', mesaSchema);

module.exports = mesa;
