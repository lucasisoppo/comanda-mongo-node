const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const pedidoSchema = new Schema({
    codigo: {
      type: String,
      required: true
    },
    emissao: {
      type: Date,
      default: Date.now,
      required: true
    },
    mesa: {
      type: Schema.Types.ObjectId,
      ref: 'mesas'
    },
    itens: [{
      codigo: { type: String, required: true },
      descricao: String
    }]
  }, { timestamps: true });
const pedido = mongoose.model('pedidos', pedidoSchema);

module.exports = pedido;