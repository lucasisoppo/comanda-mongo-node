const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
     codigo: {
        type: String,
        required: true
    },
    descricao: {
        type: String,
        required: true
    },
    preco: Number,
    estoque: Number
}, {timestamps: true});
const item = mongoose.model('itens', itemSchema);

module.exports = item;