(function() {

    angular.module('minhaApp')
        .factory('ItensService', ItensService);

    ItensService.$inject = ['$http'];

    function ItensService($http) {

        var _URL = '/itens';

        function findAll () {
            return $http.get(_URL)
                .then(function (response) {
                    return response.data;
                });
        }

        function findById (id) {
            return $http.get(_URL + '/' + id)
                .then(function (response) {
                    return response.data;
                });
        }

        function insert (item) {
            return $http.post(_URL, item)
                .then(function (response) {
                    return response.data;
                });
        }

        function update (item) {
            return $http.put(_URL + '/' + item._id, item)
                .then(function (response) {
                    return response.data;
                });
        }

        function remove (id) {
            return $http.delete(_URL + '/' + id);
        }

        return {
            findAll: findAll,
            findById: findById,
            insert :insert,
            update: update,
            remove: remove
        };
    }
})();