(function () {

    angular.module('minhaApp')
        .controller('ItensListController', ItensListController);

    ItensListController.$inject = ['ItensService']

    function ItensListController (ItensService) {
        var vm = this;
        vm.registros = [];

        vm.load = function () {
            ItensService.findAll()
            .then(function (registros) {
                vm.registros = registros;
            });
        };

        vm.load();
        
        vm.excluir = function (id) {
            swal({
                title: 'Deseja realmente excluir o registro?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim!',
                cancelButtonText: 'Não tenho certeza'
            }).then(function (result) {
                if (result.value) {
                    ItensService.remove(id)
                        .then(function() {
                            vm.load()
                            swal({
                                position: 'top-end',
                                type: 'success',
                                title: 'Item excluído com sucesso',
                                showConfirmButton: false,
                                timer: 1500
                              });
                        });
                }
            });
        }

    }
})();