(function () {

    angular.module('minhaApp')
        .controller('ItensFormController', ItensFormController);

    ItensFormController.$inject = ['ItensService', '$state', '$stateParams']

    function ItensFormController(ItensService, $state, $stateParams) {
        var vm = this;
        vm.item = {};
        vm.titulo = 'Inserindo item';

        if ($stateParams.id) {
            vm.titulo = 'Editando item';
            ItensService.findById($stateParams.id)
                .then(function (item) {
                    vm.item = item;
                });
        }

        vm.salvar = function () {
            if (vm.item._id) {
                ItensService.update(vm.item)
                    .then(function () {
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'Item atualizado com sucesso',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $state.go('app.itens')
                    });
            } else {
                ItensService.insert(vm.item)
                    .then(function () {
                        swal({
                            position: 'top-end',
                            type: 'success',
                            title: 'Item inserido com sucesso',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        $state.go('app.itens')
                    });
            }
        };
    }
})();