(function(){
    angular.module('minhaApp', [
        'ui.router'
    ]);

    angular.module('minhaApp')
        .config(MinhaAppConfig);
    
    MinhaAppConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
    
    function MinhaAppConfig($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state({
                name: 'login', 
                url: '/login',
                templateUrl: '/views/login.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app', 
                url: '/',
                templateUrl: '/views/menu.html'
            })
            .state({
                name: 'app.itens', 
                url: 'itens',
                template: '<ui-view></ui-view>',
                redirectTo: 'app.itens.list'
            })
            .state({
                name: 'app.itens.list', 
                url: '/list',
                templateUrl: '/views/itens/list.html',
                controller: 'ItensListController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.itens.new', 
                url: '/new',
                templateUrl: '/views/itens/form.html',
                controller: 'ItensFormController',
                controllerAs: 'vm'
            })
            .state({
                name: 'app.itens.edit', 
                url: '/:id',
                templateUrl: '/views/itens/form.html',
                controller: 'ItensFormController',
                controllerAs: 'vm'
            })
    
    }
})();
